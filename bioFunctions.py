# Coursera methods for bioinformatics
import itertools


def hammingDistance(gen1, gen2):
    """Compute the character mismarches
    in each string. """

    mismatch_count = 0
    for g1, g2 in zip(gen1, gen2):
        if g1 != g2:
            mismatch_count += 1

    return mismatch_count


def approximatePatternMatching(pattern, genome, d):
    """Find the approximate d
    mismatches """

    count, k = 0, len(pattern)
    distance = 0
    count_list = []

    for i in range(len(genome) - k+ 1):
        distance = hammingDistance(pattern, genome[i:i+k])
        if(distance <= d):
            count_list.append(i)

    return count_list


def computeSkew(genome):
    """Compute the skew of the genome"""
    skew = 0
    skew_list = []
    skew_list.append(0)

    for i in range(len(genome)):
        if genome[i] == 'C':
            skew -= 1
            skew_list.append(skew)

        elif genome[i] == 'G':
            skew += 1
            skew_list.append(skew)

        else:
            skew_list.insert(i,skew)


    print(skew_list)


def computeSkewMinimum(genome):
    """Find the skew minimum
    put the minimum in a list and
    return the result """

    min_skew = 0
    skew = 0
    skew_list = []

    for i in range(len(genome)):
        if genome[i] == 'C':
            skew -= 1
            if skew < min_skew:
                min_skew = skew
                skew_list = []

        elif genome[i] == 'G':
            skew += 1

        if skew == min_skew:
            skew_list.append(i+1)

    print (skew_list)




def pattternCount(text, pattern):
    count, k = 0, len(pattern)

    for i in range (len(text) - k + 1):
        if text[i:i+k] == pattern:
            count += 1
    return count


def patternToNumber(pattern):
    patterns = [''.join(x) for x in itertools.product(['A', 'C', 'G', 'T'], repeat=len(pattern))]
    return patterns.index(pattern)


def numberToPattern(number,k):
    patterns = [''.join(x) for x in itertools.product(['A', 'C', 'G', 'T'], repeat=k)]
    return patterns[number]


def computefrequency(text, k):
    result = [0 for i in range(4 ** k)]
    for i in range(len(text) - k + 1):
        result[patternToNumber(text[i:i+k])] += 1
    return result


def computeFrequencies(text, k):
    result = [0 for i in range(4 ** k)]
    for i in range(len(text) - k + 1):
     #   print("[*] frequency: ", text[i:i+k])
        result[patternToNumber(text[i:i+k])] += 1
    return result


def fasterFrequentWords(text, k):
    frequentpatterns, frequencyarray = [], computefrequency(text, k)
    maxCount = max(frequencyarray)
    for i in range(4 ** k - 1):
        if frequencyarray[i] == maxCount:
            frequentpatterns.append(text[i:i+k])
    return frequentpatterns


def fasterFrequentWordsSorting(text, k):
    wordsRange = range(len(text) - k + 1)
    index, count, result = [patternToNumber(text[i:i+k]) for i in wordsRange], [1 for i in wordsRange], []
    index.sort()
    for i in range(1, len(text) - k + 1):
        if index[i] == index[i - 1]:
            count[i] = count[i -1] + 1
    maxCount = max(count)
    for i in wordsRange:
        if count[i] == maxCount:
            result.append(numberToPattern(index[i], k))
    return result


def reverseComplement(text):
    base = ['A', 'C', 'G', 'T']
    result = [list(reversed(base))[base.index(ch)] for ch in reversed(text)]
    return ''.join(result)


def substr(pattern, genome):
    k, result = len(pattern), []
    for i in range(len(genome) - k + 1):
        if genome[i:i+k] == pattern:
            result.append(i)
    return result


def betterClumpFinding(genome, k, l, t):

    result, clump = set(), [0 for i in range(4 ** k)]
    frequencyarray = computefrequency(genome[:l], k)

    for i in range(4 ** k):
        if frequencyarray[i] >= t:
            clump[i] = 1
    for i in range(1, len(genome) - l + 1):
        frequencyarray[patternToNumber(genome[i-1:i+k-1])] -= 1
        j = patternToNumber(genome[i+l-k:i+l])
        frequencyarray[j] += 1
        if frequencyarray[j] >= t:
            clump[j] = 1
    for i in range(4 ** k):
        if clump[i] == 1:
            result.add(numberToPattern(i, k))
    return result

def main():

    dna_string = "TAAAGACTGCCGAGAGGCCAACACGAGTGCTAGAACGAGGGGCGTAAACGCGGGTCCGAT"
    gen1 = "CCGAAGCAATTGAAACCCCCCCGGCCTGGGAGGCGCAAAAATCTGACCTCTTTGTGAGTTGACCACTTAATTTATGTCTGACCACGAGAAGGGCTACTGATTTGGTACGTCGGGTCATGACCCCCAGTTCTTAGCCGCCTGCTCCAATCTCTGACTTGTTTATCGAGGGGATGGAGTAACGAAATGCGATTCGCCCGCTCAGGCCAAGGTATATATTTGAGTAGCGGAAGGTTGCACTACCTACAACCACGGCACACCGGCACGTTGTCGTGCCCTGGCGGCCTGCGCACTTTCGCCACTGTCAAGTACGACTTCCCAAGCTCAACCAACATTCATAATCCGGTGCAATTCATACCGTATCATCGTGCTATAAGCGACGCCGATTCTCGGGGCCTGATAATTGAGACTGGACTACATAGTGGGTGCCCTCTCTGCGAGTAAGTGACGGAACAACGGAGATCAGGGACCAAATGGTAGCAAAACAGATCGAGGTACACGCAGGTAGCTGTCCGTGGAGTAGACCGCGCTTAGCGTCTGTTAGAGTATCATCGGGGTATTAGACACAGGAACCTCTATGCTGTTAAAAGGCCATACCCCGTAATTGTGCAAATTTGTTACGTTCAAATCTACGCAGTGAGGGTCCTAAGGTGATGGCAGGGATTGGAACTTCTCCGCTGGCTCTTAGATTACTTAGCCAGTCTACCCTCGAAGATACAAATCCTTCCACCAGAGGGAGCTCATTGAAATTCATTCCATGCTACTCGACCGCGCGTATGGGTGCGGGGCTCTATGGGATCTAACTCGATCCTTCAGAGTCCTTATTCAAATGCATTTCCGTCCCCGTATGTTTCGACGAAGCCGAAGCCCAAACCCTGGGATGGACGAATTAAGGACAGTACAGGCAATAGTGTTCTCCCATACTCGGAACAGACGCCTCATTTTTTCGCGAAATCGATCTGGGTTGGAAGAAGTTCCAGTGCAGAGTTCCTATCACACAATTCGTTCTCGGGGCTTCCGGCCCATAAGCGATACTACTGTCTTTGCGAGCTAACGATTACATTCGGGGGAACTTAGCTCGGACTGGACCAGGTACATGATCCAAAGCGCGATGTCTGTCTGTTACCCTCACCGCCGCTCTTTTATCGGGTA"

    gen2 = "GCGTAGTAGGTTCGCGTACCTAGTTCCGCCGAAAAGACAAAGGAGAAGGGAATGCTCCTAGTAGTTTCAGTCTAGCAAACATGTTATAACGCTAACTGTGTGCTGCAAAAAGGATTTGAACCCAAATTTTAAAGCGCTGATCGACAGAACGCTGTTGAAGAGGCGATGGTACTGAGATTCCCCAGAAACCACCTCCGCGCTATGTGCTCAAGACAACCCGCATTCGTTTTTACTAGATTTGGAGCCGAGTTGTGATTTGGATATTTTCACATAAGACCGAGCAGGAAATATACCTTGTTGCAGCTATTGACCCCGTTCTCTCGGAAATCCATGGAATAGTCTTCGGATATTCGTACCAATGGGCGCGATGTTGCGATAAGAGAGCACATTTCATTAAGTGGTGCTCCGCCGCTAAGATGGGAAGGGGCGAGTCTATCGCAGCATCGAAGGCTGAGTTGGCCATTGCCGAGAGTATACATATTTACGATCACACTCGCATAGTCCCACGCATTACGTCCGAGATAGTATGTCCCAATGCAACCTAAAGCCGCGAGATTCCCTAAGGAGAAAATTAAACACTGGAAATTAGGTGATGCTACATCCCATGGACACTTTCGGAACAATATCGGTGACACACATCATCCGTGATCCCGTGATATTTCATCCATGGAGAGAGTATGGTTTTACTACACCTGGTCTAGGCCAAGCCTAACCCCCTGTTCATCCGTTTTATACGAGTATTACCTTGACGACCATAGAGGATAGACTCGGTATCCCGCACACTCTACACACACGACTTAATCCGCTCCACGACCTTCCTAGCGATCTTTGGCGCAGCCGGTTCGCGTATTTTACGACCAACTCGATGGATCCCAATTATCCCCCTGGTAGTGCCCCTCCGCCTGAGAATTCGACGGGCGAGGTCCGGGGGACCGACATAGAGTGGAATGCTTCTTTCCGGGATAACACGTGATTGACATAAAAATGTAGGGCAGATAGGCATCGTTAGCACCTCTCTCCTTGCTGCACTGCGTTTATCGATCGAATTCAAGACTTGTGCATGTTGAAAACAACCTCGCGTTATCCCTGCTATTTGCTTCAGAGCCGTAGGAGGGGACCATGCGTGAGTCCTCCTGAGCAACCTCAATT"

    res =  hammingDistance(gen1, gen2)

    print (res)

    result = approximatePatternMatching("ATTCTGGA","CGCCCGAATCCAGAACGCATTCCCATATTTCGGGACCACTGGCCTCCACGGTACGGACGTCAATCAAAT",3)
    print(result)
    # a = dna_string[:int(mid)]
    # b = dna_string[int(mid + 1):]
    # overlap = dna_string[]
    # print (a, "\n")
    # print(b, "\n")

    file_ptr = open("data_set.txt", "r")

    dna_seq = file_ptr.read()

    pattern = "TGT"
    k = 2

    #Test the various functions.

    # test the pattern count function
 #   print("[*] Pattern count: ", pattternCount(dna_string, pattern))

    # test patternToNumber method
 #   print("[*] Pattern to number: ",patternToNumber(pattern))

    # test numberToPattern function
  #  print("[*] Number to pattern: ",numberToPattern(11, k))

    # test compute frequencies
  #  print("[*] Computing frequencies: ", computeFrequencies(dna_string, 9))

    #test faster frequent words
  #  print("[*] Testing Faster Frequent Word....", fasterFrequentWords(dna_string, 2))

    # test faster frequent words by sorting
  #  print("[*] Testing faster frequent words by sorting...", fasterFrequentWordsSorting(dna_string, 4))

    # test reverse complement
  #  print("[*] Testing reverse complement...", "AGT" , reverseComplement(pattern))

   # computeSkew(dna_string)
    # test clump finding  seq, k.l.t
    # print("[*] Testing clump finding...", betterClumpFinding(dna_string, 3, 10, 2))

    #computeSkewMinimum(dna_string)

if __name__ == "__main__":
    main()

